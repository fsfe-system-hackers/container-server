#!/bin/bash

# SPDX-FileCopyrightText: 2022 Free Software Foundation Europe <https://fsfe.org>
#
# SPDX-License-Identifier: AGPL-3.0-or-later

# Backup Docker containers' databases etc
/root/bin/backup-containers.sh
