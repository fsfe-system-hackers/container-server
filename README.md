<!--
SPDX-FileCopyrightText: 2021 Free Software Foundation Europe <https://fsfe.org>

SPDX-License-Identifier: AGPL-3.0-or-later
-->

# Container Server Playbook

[![REUSE
status](https://api.reuse.software/badge/git.fsfe.org/fsfe-system-hackers/container-server)](https://api.reuse.software/info/git.fsfe.org/fsfe-system-hackers/container-server) [![Build
Status](https://drone.fsfe.org/api/badges/fsfe-system-hackers/container-server/status.svg)](https://drone.fsfe.org/fsfe-system-hackers/container-server)

Configure a host to run containers

<!-- markdown-toc start - Don't edit this section. Run M-x markdown-toc-refresh-toc -->

**Table of Contents**

- [Container Server Playbook](#container-server-playbook)
  - [Background](#background)
  - [Security](#security)
  - [Install](#install)
    - [Gotchas](#gotchas)
  - [Usage](#usage)
    - [Configuration](#configuration)
    - [Additional Resources](#additional-resources)

<!-- markdown-toc end -->

## Background

Not every host we have needs to run containers. If it does, however, this
playbook is the place to start. Currently, it does the following:

- sets up Docker in [rootless
  mode](https://docs.docker.com/engine/security/rootless/)
- configures [Caddy](roles/caddy-rp) with automatic certificate generation to
  act as a reverse proxy, with
  [docker2caddy](https://git.fsfe.org/fsfe-system-hackers/docker2caddy/) for
  automatic config generation based on container labels
- configures a [Drone Docker
  runner](https://docs.drone.io/runner/docker/overview/)
- sets up backup steps and cron jobs

## Security

This playbook repository contains secrets that are encrypted using the [Ansible
vault](https://docs.ansible.com/ansible/latest/user_guide/vault.html). The
passphrase to decrypt the vault lives in the GPG-encrypted file
[`vault_passphrase.gpg`](vault_passphrase.gpg). If you need access to the
encrypted parts of this playbook or you want to be able to encrypt variables
whilst setting up a new host, simply create an issue in this repository and we
will review your request.

## Install

You need a host that runs at least Debian 11 and an up-to-date version of our
[inventory](https://git.fsfe.org/fsfe-system-hackers/inventory), the
latter of which can be attained by running:

```shell
git clone --recurse-submodules git@git.fsfe.org:fsfe-system-hackers/container-server.git
```

or when you have already cloned the repository without recursing into
submodules:

```shell
git submodule update --remote inventory
```

Next, you obviously need Ansible. To obtain and use the version of Ansible this
playbook is tested with (2.11), obtain a working version of
[`pipenv`](https://pipenv.pypa.io/en/latest/#install-pipenv-today) and run:

```shell
pipenv install
pipenv shell
```

### Gotchas

The current version of `fuse-overlayfs` in Debian Buster [was too
old](https://www.reddit.com/r/docker/comments/jcf8ho/rootless_docker_save_not_working/?utm_source=share&utm_medium=web2x&context=3)
(0.3-1) and caused problems when building images, e.g. via `docker-compose` in
rootless mode. After installing the version which will be available in Bullseye
(1.4.1), the problem was fixed and all builds succeeded. This will be monitored.

## Usage

In order to ...

- install the container engine (currently Docker in [rootless
  mode](https://docs.docker.com/engine/security/rootless/)) along with
  `docker-compose` and `ctop`,
- configure [Caddy](https://caddyserver.com/) as a reverse proxy, and
  [docker2caddy](https://git.fsfe.org/fsfe-system-hackers/docker2caddy/) to
  automatically create config files,
- configure the [Drone Docker
  runner](https://docs.drone.io/runner/docker/overview/) and connect it to the
  FSFE's [Drone server](https://drone.fsfe.org),
- create necessary [sudo permissions](roles/ansible-sudo) for our monitoring
  system to be able to read health information,
- create [pre-backup tasks](roles/ansible-docker-backup) to be run before the
  regular backup, and to set up [cron jobs](roles/ansible-docker-cron),

you need to do two things:

1. Apply the labels `container_server` to the desired hosts in the
   [inventory](inventory/inventory.txt).
2. Then, simply run:

```shell
ansible-playbook playbook.yml
```

If you just want to install the container engine, run:

```shell
ansible-playbook playbook.yml --tags container-engine
```

### Configuration

The default relevant variables are configured in the [`group_vars`
directory](group_vars), but some variables are host-specific. Simply create a
file in the [`host_vars` directory](host_vars) called `<hostname>.yml`, e.g.
[`host_vars/cont1.noris.fsfeurope.org.yml`](host_vars/cont1.noris.fsfeurope.org.yml).
So far, the following configuration is tested to work on a fresh and minimal
Debian 11 install.

### Additional Resources

Third-party Ansible roles used in this playbook are:

- [ansible-docker-rootless](https://github.com/konstruktoid/ansible-docker-rootless)
  by Thomas Sjögren
